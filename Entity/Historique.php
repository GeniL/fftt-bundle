<?php

namespace Al\FFTTBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historique
 *
 * @ORM\Table(name="historique")
 * @ORM\Entity(repositoryClass="Al\FFTTBundle\Repository\HistoriqueRepository")
 */
class Historique
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="anneeDebut", type="integer")
     */
    private $anneeDebut;

    /**
     * @var int
     *
     * @ORM\Column(name="anneeFin", type="integer")
     */
    private $anneeFin;

    /**
     * @var int
     *
     * @ORM\Column(name="phase", type="integer")
     */
    private $phase;

    /**
     * @var int
     *
     * @ORM\Column(name="points", type="integer")
     */
    private $points;

    /**
     * @ORM\ManyToOne(targetEntity="Al\FFTTBundle\Entity\Joueur", inversedBy="historiques")
     * @ORM\JoinColumn(nullable=false)
     */
    private $joueur;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set anneeDebut
     *
     * @param integer $anneeDebut
     *
     * @return Historique
     */
    public function setAnneeDebut($anneeDebut)
    {
        $this->anneeDebut = $anneeDebut;

        return $this;
    }

    /**
     * Get anneeDebut
     *
     * @return int
     */
    public function getAnneeDebut()
    {
        return $this->anneeDebut;
    }

    /**
     * Set anneeFin
     *
     * @param integer $anneeFin
     *
     * @return Historique
     */
    public function setAnneeFin($anneeFin)
    {
        $this->anneeFin = $anneeFin;

        return $this;
    }

    /**
     * Get anneeFin
     *
     * @return int
     */
    public function getAnneeFin()
    {
        return $this->anneeFin;
    }

    /**
     * Set phase
     *
     * @param integer $phase
     *
     * @return Historique
     */
    public function setPhase($phase)
    {
        $this->phase = $phase;

        return $this;
    }

    /**
     * Get phase
     *
     * @return int
     */
    public function getPhase()
    {
        return $this->phase;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return Historique
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    public function getJoueur() : Joueur
    {
        return $this->joueur;
    }

    public function setJoueur(Joueur $joueur)
    {
        $this->joueur = $joueur;
    }
}

