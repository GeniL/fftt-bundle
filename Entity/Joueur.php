<?php

namespace Al\FFTTBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Joueur
 *
 * @ORM\Table(name="joueur")
 * @ORM\Entity(repositoryClass="Al\FFTTBundle\Repository\JoueurRepository")
 */
class Joueur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="licence", type="integer", unique=true)
     */
    private $licence;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @ORM\OneToMany(targetEntity="Al\FFTTBundle\Entity\Classement", mappedBy="joueur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $classements;

    /**
     * @ORM\OneToMany(targetEntity="Al\FFTTBundle\Entity\Historique", mappedBy="joueur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $historiques;


    /**
     * @ORM\OneToMany(targetEntity="Al\FFTTBundle\Entity\Partie", mappedBy="joueur")
     * @ORM\JoinColumn(nullable=false)
     */
    private $parties;

    public function __construct()
    {
        $this->classements = new ArrayCollection();
        $this->parties = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set licence
     *
     * @param integer $licence
     *
     * @return Joueur
     */
    public function setLicence($licence)
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * Get licence
     *
     * @return int
     */
    public function getLicence()
    {
        return $this->licence;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Joueur
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Joueur
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    public function getClassements()
    {
        return $this->classements;
    }

    public function setClassements($classements)
    {
        $this->classements = $classements;
    }

    public function addClassement(Classement $classement){
        $this->classements->add($classement);
        $classement->setJoueur($this);
        return $this;
    }

    public function getLatestClassement() : ?Classement {
        return $this->classements->count() ? $this->classements->last() : null;
    }

    public function getParties()
    {
        return $this->parties;
    }

    public function setParties($parties)
    {
        $this->parties = $parties;
    }


    /**
     * @return Historique[]
     */
    public function getHistoriques()
    {
        return $this->historiques;
    }

    public function setHistoriques($historiques)
    {
        $this->historiques = $historiques;
    }
}

