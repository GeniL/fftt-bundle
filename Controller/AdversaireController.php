<?php

namespace Al\FFTTBundle\Controller;

use Al\FFTTBundle\Entity\Joueur;
use FFTTApi\Exception\JoueurNotFound;
use FFTTApi\Exception\NoFFTTResponseException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class AdversaireController extends Controller implements ReloadCache
{
    /**
     * @ParamConverter("joueur")
     */
    public function joueurAction(Joueur $joueur, Request $request)
    {
        $id = $request->get('adversaire');
        $api = $this->get('fftt.api');
        $adversaire = $api->getJoueurDetailsByLicence($id);
        $parties = $api->getPartiesJoueurByLicence($id);

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('joueur.details')
            ->setRouteParameters([
                'id' => $joueur->getId(),
            ])
            ->setLabelParameters([
                '%name%' => $joueur->getPrenom()." ".$joueur->getNom(),
            ]);

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('joueur.adversaire')
            ->setLabelParameters([
                '%name%' => $adversaire->getPrenom()." ".$adversaire->getNom(),
            ]);

        return $this->render('@AlFFTT/adversaire/joueur.html.twig',[
            'joueur' => $adversaire,
            'parties' => $parties
        ]);
    }

    public function adversaireAction(Request $request)
    {
        $id = $request->get('id');

        $api = $this->get('fftt.api');
        $adversaire = $api->getJoueurDetailsByLicence($id);
        $parties = $api->getPartiesJoueurByLicence($id);

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('adversaire.details')
            ->setLabelParameters([
                '%name%' => $adversaire->getPrenom()." ".$adversaire->getNom(),
            ]);

        return $this->render('@AlFFTT/adversaire/joueur.html.twig',[
            'joueur' => $adversaire,
            'parties' => $parties
        ]);
    }

    public function searchAction(Request $request)
    {
        $nom =  $request->get("nom");
        $prenom =  $request->get("prenom");

        $joueurs = [];
        try{
            $joueurs = $this->get('fftt.api')->getJoueursByNom($nom, $prenom);
        }catch (NoFFTTResponseException $e){}

        if(count($joueurs) == 1){
            return $this->redirectToRoute("adversaire.details", [
                "id" => $joueurs[0]->getLicence()
            ]);
        }
        elseif (count($joueurs)){
            $this->addFlash("error", "Impossible de trouver ce joueur");
            return new RedirectResponse($this->generateUrl("adversaire.search"));
        }

        return $this->render('@AlFFTT/adversaire/chooseSearch.html.twig', [
            'joueurs' => $joueurs
        ]);
    }

    public function searchFormAction(Request $request){
        $form = $this->createFormBuilder()
            ->add("lastName", TextType::class,
                [
                    "label" => "Nom",
                    "required" => false,
                ]
            )->add("firstName", TextType::class,
                [
                    "label" => "Prénom",
                    "required" => false,
                ]
            )
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data =  $form->getData();
            try{
                $joueurs = $this->get('fftt.api')->getJoueursByNom($data["lastName"] ?? "", $data["firstName"] ?? "");

                if(count($joueurs) == 1){
                    return $this->redirectToRoute("adversaire.details", [
                        "id" => $joueurs[0]->getLicence()
                    ]);
                }
                return $this->render('@AlFFTT/adversaire/chooseSearch.html.twig', [
                    'joueurs' => $joueurs
                ]);
            }
            catch (NoFFTTResponseException $e){
                $this->addFlash("error", "Aucun joueur de trouvé");
            }
        }

        return $this->render('@AlFFTT/adversaire/search.html.twig',
            [
                "form" => $form->createView()
            ]);
    }

    public function clubAction(Request $request){
        $id = $request->get('id');

        return $this->render('@AlFFTT/adversaire/club/club.html.twig', [
            "id" => $id
        ]);
    }

    public function clubJoueursAction(Request $request){
        $id = $request->get('id');
        $api = $this->get('fftt.api');

        $joueurs = $api->getJoueursByClub($id);

        return $this->render('@AlFFTT/adversaire/club/joueurs.html.twig',[
            'joueurs' => $joueurs
        ]);
    }

    public function clubDetailsAction(Request $request){
        $id = $request->get('id');
        $api = $this->get('fftt.api');

        $club = $api->getClubDetails($id);

        return $this->render('@AlFFTT/adversaire/club/details.html.twig',[
            'club' => $club
        ]);
    }

}
