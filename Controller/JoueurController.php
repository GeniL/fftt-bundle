<?php

namespace Al\FFTTBundle\Controller;

use Al\FFTTBundle\Entity\Historique;
use Al\FFTTBundle\Entity\Joueur;
use Al\FFTTBundle\Entity\Partie;
use FFTTApi\Exception\JoueurNotFound;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class JoueurController extends Controller implements ReloadCache
{
    public function listJoueursAction()
    {
        $joueurs = $this->getDoctrine()->getRepository(Joueur::class)->findAll();
        return $this->render('@AlFFTT/Joueurs/list.html.twig',[
            'joueurs' => $joueurs
        ]);
    }

    /**
     * @param Joueur $joueur
     * @ParamConverter("joueur")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsJoueurAction(Joueur $joueur){

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('joueur.details')
            ->setRouteParameters([
                'id' => $joueur->getId(),
            ])
            ->setLabelParameters([
                '%name%' => $joueur->getPrenom()." ".$joueur->getNom(),
            ]);

        $datas = $this->getDoctrine()->getRepository(Partie::class)->sumByDay($joueur);

        $points =[];
        $xAxis =[];
        $lastPoints = $joueur->getLatestClassement()->getPointsInitials();
        foreach ($datas as $data){
            $lastPoints +=intval($data['somme']);
            $points[] = $lastPoints;
            $xAxis[] = $data['gbd']."/".$data['gbm']."/".$data['gby'];
        }

        return $this->render('@AlFFTT/Joueurs/details.html.twig',[
            'joueur' => $joueur,
            'points' => json_encode($points),
            'xAxis' => json_encode($xAxis)
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @ParamConverter("joueur")
     */
    public function notValidatedMatchJoueurAction(Request $request){
        $joueurLic = $request->get("id");
        $forceReload = $request->get("reload");

        $cache = $this->get('fftt.cache');
        $key = "notValidatedMatch-".$joueurLic;

        if(true != ($forceReload == "true")  and $cache->has($key)){
            $parties = $cache->get($key);
        }
        else{
            $parties = [];
            try{
                $joueurData = $this->get('fftt.api')->getClassementJoueurByLicence($joueurLic);
                foreach ($this->get('fftt.api')->getUnvalidatedPartiesJoueurByLicence($joueurLic) as $partie){
                    $virtualPointWithoutCoeff = $partie->isVictoire() ?
                        $this->get('fftt.pointCalculator')
                            ->getPointVictory($joueurData->getPoints(), $partie->getAdversaireClassement()
                            ) :
                        $this->get('fftt.pointCalculator')
                            ->getPointDefeat($joueurData->getPoints(), $partie->getAdversaireClassement()
                            );

                    $parties[] = [
                        "partie" => $partie,
                        "virtualPoint" =>  $virtualPointWithoutCoeff * $this->get('fftt.pointCalculator')->getCoefficientOfEpreuve($partie->getEpreuve())
                    ];
                }
            }
            catch (JoueurNotFound $e){
                $this->get("logger")->addError(sprintf(
                    "Impossible de récuperer les parties non validées du joueur %s",
                    $joueurLic
                ));
            }

            $cache->set($key, $parties);
        }

        return $this->render("@AlFFTT/Joueurs/notValidatedMatch.html.twig", [ "parties" => $parties]);
    }

    public function virtualPointsJoueurAction(Request $request){
        $id = $request->get("id");
        $forceReload = $request->get("reload");

        $cache = $this->get('fftt.cache');
        $key = "virtualPoints-".$id;

        if(true != ($forceReload == "true") and $cache->has($key)){
            $virtualPoints = $cache->get($key);
        }
        else{
            try{
                $joueurData = $this->get('fftt.api')->getClassementJoueurByLicence($id);
                $virtualPoints = $joueurData->getPoints() + $this->get('fftt.api')->getVirtualPoints($id);
            }
            catch (JoueurNotFound $e){
                $virtualPoints = "NOK";
            }
            $cache->set($key, $virtualPoints);
        }

        return new Response($virtualPoints);
    }

    /**
     * @param Joueur $joueur
     * @ParamConverter("joueur")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function historiqueJoueurAction(Joueur $joueur){

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('joueur.details')
            ->setRouteParameters([
                'id' => $joueur->getId(),
            ])
            ->setLabelParameters([
                '%name%' => $joueur->getPrenom()." ".$joueur->getNom(),
            ]);

        $points =[];
        $xAxis =[];
        /** @var Historique $historique */
        foreach ($joueur->getHistoriques() as $historique){
            $points[]=$historique->getPoints();
            $xAxis[]= sprintf(
                "%s - %s (%s)",
                $historique->getAnneeDebut(),
                $historique->getAnneeFin(),
                $historique->getPhase()
            );
        }

        return $this->render('@AlFFTT/Joueurs/historique.html.twig',[
            'joueur' => $joueur,
            'points' => json_encode($points),
            'xAxis' => json_encode($xAxis)
        ]);
    }
}
