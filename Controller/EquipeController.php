<?php

namespace Al\FFTTBundle\Controller;

use Al\FFTTBundle\Entity\Equipe;
use FFTTApi\Exception\InvalidLienRencontre;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class EquipeController extends Controller implements ReloadCache
{
    public function listAction()
    {
        $equipes = $this->getDoctrine()->getRepository(Equipe::class)->findAll();
        return $this->render('@AlFFTT/Equipes/list.html.twig',[
            'equipes' => $equipes
        ]);
    }

    /**
     * @ParamConverter("equipe")
     */
    public function detailsAction(Equipe $equipe){

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('equipe.details')
            ->setRouteParameters([
                'id' => $equipe->getId(),
            ])
            ->setLabelParameters([
                '%name%' => $equipe->getLibelle(),
            ]);


        $classements = $this->get('fftt.api')->getClassementPouleByLienDivision($equipe->getLienDivision());
        $rencontres = $this->get('fftt.api')->getRencontrePouleByLienDivision($equipe->getLienDivision());
        $rencontresGrouped = [];

        foreach ($rencontres as $rencontre ) {
            foreach ($classements as $equipePoule){
                if($rencontre->getNomEquipeA() === $equipePoule->getNomEquipe()){
                    $rencontre->setClubEquipeA($equipePoule->getNumero());
                }
                if($rencontre->getNomEquipeB() === $equipePoule->getNomEquipe()){
                    $rencontre->setClubEquipeB($equipePoule->getNumero());
                }
            }
            $rencontresGrouped[$rencontre->getLibelle()][] = $rencontre;
        }
        return $this->render('@AlFFTT/Equipes/details.html.twig',[
            'equipe' => $equipe,
            'classements' => $classements,
            'rencontresGrouped' => $rencontresGrouped,
        ]);
    }

    /**
     * @ParamConverter("equipe")
     */
    public function rencontreAction(Equipe $equipe, Request $request){
        $lien = $request->get('lien');
        $forceReload = $request->get("reload");

        $clubEquipeA = $request->get('clubEquipeA');
        $clubEquipeB = $request->get('clubEquipeB');

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('equipe.details')
            ->setRouteParameters([
                'id' => $equipe->getId(),
            ])
            ->setLabelParameters([
                '%name%' => $equipe->getLibelle(),
            ]);


        $cache = $this->get('fftt.cache');
        $keyValue = 'rencontre-'.$lien;

        if(true != ($forceReload == "true") and$cache->has($keyValue)){
            $rencontre = $cache->get($keyValue);
        }
        else{
            try{
                $rencontre = $this->get('fftt.api')->getDetailsRencontreByLien($lien, $clubEquipeA, $clubEquipeB);
                $cache->set($keyValue, $rencontre);
            }
            catch(InvalidLienRencontre $e) {
                $this->get('thormeier_breadcrumb.breadcrumb_provider')
                    ->getBreadcrumbByRoute('rencontre.details')
                    ->setLabelParameters([
                        '%name%' => "Rencontre non trouvée",
                    ]);

                return $this->render('@AlFFTT/Equipes/rencontreNotFound.html.twig');
            }

        }

        $this->get('thormeier_breadcrumb.breadcrumb_provider')
            ->getBreadcrumbByRoute('rencontre.details')
            ->setLabelParameters([
                '%name%' => $rencontre->getNomEquipeA()." - ".$rencontre->getNomEquipeB(),
            ]);


        return $this->render('@AlFFTT/Equipes/rencontre.html.twig',[
            'rencontre' => $rencontre
        ]);
    }
}
