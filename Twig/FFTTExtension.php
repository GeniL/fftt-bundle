<?php
/**
 * Created by Antoine Lamirault.
 */

namespace Al\FFTTBundle\Twig;

class FFTTExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction(
                'formatPlayerName',
                [$this, 'formatPlayerName'],
                ['needs_environment' => true]
            ),
        );
    }

    public function formatPlayerName(\Twig_Environment $environment, string $nom, string $prenom)
    {
        $template =  $environment->createTemplate("{{ nom | upper ~' '~ prenom | capitalize}}");
        return $template->render(
            [
                "nom" => $nom,
                "prenom" => $prenom,
            ]
        );
    }
}